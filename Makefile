

up: vendor
	docker-compose up -d

down:
	docker-compose down

bash:
	@docker-compose run --rm php bash

vendor:
	docker-compose run --rm composer install