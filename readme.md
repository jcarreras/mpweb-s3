# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Práctica para entender el uso de S3 para subir ficheros y compartirlos públicamente


# Instalación
-----------------------


Ejecuta:
```
$ make up
```

Entra en el container ejecutando:
```
make bash
```


Si estás en un Linux tus credenciales de ~/.aws se compartirán automáticamente dentro del container


Si estás en Windows, tendrás que declarar dos variables de entorno para configurar tus credenciales
de AWs como se explica en la documentación.


# Instrucciones
-----------------------

- Abre el fichero `upload_file_s3.php`
- Allí hay dos secciones a rellenar:
- La primera sección es rellenar la variable `$YOUR_BUCKET_NAME` con el bucket con el que quieras hacer la práctica (puede tener cualquier nombre)
- En la segunda sección tienes que completar el código que falta para subir el fichero `wallpaper1.jpg` al bucket especificado a S3
- Puedes ir haciendo pruebas ejecutando `php upload_file_s3.php` y deberías ver la URL pública de la imágen como resultado si todo ha ido bien


# Desinstalación
-----------------------

```
make down
```

- Borra la imagen subida a S3 manualmente entrando en la consola de AWS (https://console.aws.amazon.com/s3/home?region=eu-west-1)